annualSummary <-
function(x, fun=mean, na.rm=TRUE, filename="", verbose=FALSE, ...){
  # outputs a rasterBrick with a summarized layer per year of a time series
  
  # assumed that layer names correspond to landsat scenenames
  # extract sceneinfo
  s <- getSceneinfo(names(x))
  
  # years vector
  s$year <- substr(s$date, 1, 4)
  years <- sort(unique(s$year))
  
  # list for annual layers
  sumVal <- list()
  
  # populate sumVal for each year in the time series
  for(i in 1:length(years)){
    if(verbose)
      cat(years[i], "...", sep="")
    sumVal[[i]] <- calc(x[[which(s$year==years[i])]], fun=fun, na.rm=na.rm)
  }
  if(verbose)
    cat("done.\n")
  
  # coerce to rasterBrick
  sumVal <- do.call("brick", sumVal)
  
  # layer names corresponding to the years
  names(sumVal) <- years
  
  # write to file if filename is supplied
  if(filename!="")
    writeRaster(sumVal, filename=filename, ...)
  
  return(sumVal)
}
