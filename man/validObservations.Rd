\name{validObservations}
\alias{validObservations}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
Calculate number of valid observations per pixel in a raster Brick.
}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
validObservations(x, sensor = "all", as.perc = FALSE, cpus = 1, filename = "")
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{x}{
%%     ~~Describe \code{x} here~~
}
  \item{sensor}{
%%     ~~Describe \code{sensor} here~~
}
  \item{as.perc}{
%%     ~~Describe \code{as.perc} here~~
}
  \item{cpus}{
%%     ~~Describe \code{cpus} here~~
}
  \item{filename}{
%%     ~~Describe \code{filename} here~~
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
Ben DeVries, Jonathan Greenberg, Jan Verbesselt}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (x, sensor = "all", as.perc = FALSE, cpus = 1, filename = "") 
{
    if (sensor != "all") {
        s <- getSceneinfo(names(x))
        if ("ETM+" \%in\% sensor) {
            sensor <- unique(c(sensor, "ETM+ SLC-on", "ETM+ SLC-off"))
        }
        x <- dropLayer(x, which(!s$sensor \%in\% sensor))
        s <- s[which(s$sensor \%in\% sensor), ]
        names(x) <- row.names(s)
    }
    obs_array <- function(rasterTS, ...) {
        rasterTS_dims <- dim(rasterTS)
        npixels <- prod(rasterTS_dims[1:2])
        ndates <- rasterTS_dims[3]
        dim(rasterTS) <- c(npixels, ndates)
        obs_value <- foreach(i = seq(npixels), .combine = c) \%do\% 
            {
                y <- rasterTS[i, ]
                x <- length(y[!is.na(y)])
                if (as.perc) 
                  x <- x/ndates * 100
                return(x)
            }
        dim(obs_value) <- c(rasterTS_dims[1:2], 1)
        return(obs_value)
    }
    sfQuickInit(cpus = cpus)
    if (filename != "") 
        nobs <- rasterEngine(rasterTS = x, fun = obs_array, filename = filename)
    else nobs <- rasterEngine(rasterTS = x, fun = obs_array)
    sfQuickStop()
    nobs[nobs == 0] <- NA
    return(nobs)
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
