\name{tassCap}
\alias{tassCap}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
Landsat Tasseled Cap Transformation
}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
tassCap(b1, b2, b3, b4, b5, b7, sensor = c(4, 5, 7), index = c("all", "brightness", "greenness", "wetness"), filename = "", ...)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{b1}{
%%     ~~Describe \code{b1} here~~
}
  \item{b2}{
%%     ~~Describe \code{b2} here~~
}
  \item{b3}{
%%     ~~Describe \code{b3} here~~
}
  \item{b4}{
%%     ~~Describe \code{b4} here~~
}
  \item{b5}{
%%     ~~Describe \code{b5} here~~
}
  \item{b7}{
%%     ~~Describe \code{b7} here~~
}
  \item{sensor}{
%%     ~~Describe \code{sensor} here~~
}
  \item{index}{
%%     ~~Describe \code{index} here~~
}
  \item{filename}{
%%     ~~Describe \code{filename} here~~
}
  \item{\dots}{
%%     ~~Describe \code{\dots} here~~
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (b1, b2, b3, b4, b5, b7, sensor = c(4, 5, 7), index = c("all", 
    "brightness", "greenness", "wetness"), filename = "", ...) 
{
    if (sensor \%in\% c("ETM+", "ETM+ SLC-off", "ETM+ SLC-on")) 
        sensor <- 7
    if (sensor == "TM") {
        sensor <- 5
        warning("Using Landsat 5 coefficients. Set sensor=4 to use Landsat 4 coefficients.\n")
    }
    if ("all" \%in\% index) {
        index <- c("brightness", "greenness", "wetness")
    }
    else {
        index <- sort(unique(index))
    }
    if (!(all(index \%in\% c("brightness", "greenness", "wetness")))) 
        stop("Unrecognized index: ", index[which(!index \%in\% 
            c("brightness", "greenness", "wetness"))])
    tass_coef <- matrix(nc = 6, nr = 3)
    if (sensor == 7) {
        tass_coef[1, ] <- c(0.3561, 0.3972, 0.3904, 0.6966, 0.2286, 
            0.1596)
        tass_coef[2, ] <- c(-0.3344, -0.3544, -0.4556, 0.6966, 
            -0.0242, -0.263)
        tass_coef[3, ] <- c(0.2626, 0.2141, 0.0926, 0.0656, -0.7629, 
            -0.5388)
    }
    else if (sensor == 5) {
        tass_coef[1, ] <- c(0.3037, 0.2793, 0.4743, 0.5585, 0.5082, 
            0.1863)
        tass_coef[2, ] <- c(-0.2848, -0.2435, -0.5436, 0.7243, 
            0.084, -0.18)
        tass_coef[3, ] <- c(0.1509, 0.1973, 0.3279, 0.3406, -0.7112, 
            -0.4572)
    }
    else if (sensor == 4) {
        tass_coef[1, ] <- c(0.33183, 0.33121, 0.55177, 0.42514, 
            0.48087, 0.25252)
        tass_coef[2, ] <- c(-0.24717, -0.16263, -0.40639, 0.85468, 
            0.05493, -0.11749)
        tass_coef[3, ] <- c(0.13929, 0.2249, 0.40359, 0.25178, 
            -0.70133, -0.45732)
    }
    tassBright <- function(x1, x2, x3, x4, x5, x7) {
        x1 * tass_coef[1, 1] + x2 * tass_coef[1, 2] + x3 * tass_coef[1, 
            3] + x4 * tass_coef[1, 4] + x5 * tass_coef[1, 5] + 
            x7 * tass_coef[1, 6]
    }
    tassGreen <- function(x1, x2, x3, x4, x5, x7) {
        x1 * tass_coef[2, 1] + x2 * tass_coef[2, 2] + x3 * tass_coef[2, 
            3] + x4 * tass_coef[2, 4] + x5 * tass_coef[2, 5] + 
            x7 * tass_coef[2, 6]
    }
    tassWet <- function(x1, x2, x3, x4, x5, x7) {
        x1 * tass_coef[3, 1] + x2 * tass_coef[3, 2] + x3 * tass_coef[3, 
            3] + x4 * tass_coef[3, 4] + x5 * tass_coef[3, 5] + 
            x7 * tass_coef[3, 6]
    }
    tc <- vector("list", length(index))
    for (i in 1:length(index)) {
        if (index[i] == "brightness") {
            tc[[i]] <- overlay(b1, b2, b3, b4, b5, b7, fun = tassBright)
        }
        else if (index[i] == "greenness") {
            tc[[i]] <- overlay(b1, b2, b3, b4, b5, b7, fun = tassGreen)
        }
        else if (index[i] == "wetness") {
            tc[[i]] <- overlay(b1, b2, b3, b4, b5, b7, fun = tassWet)
        }
    }
    tc <- do.call("brick", tc)
    names(tc) <- index
    if (filename != "") 
        writeRaster(tc, filename = filename, ...)
    return(tc)
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
